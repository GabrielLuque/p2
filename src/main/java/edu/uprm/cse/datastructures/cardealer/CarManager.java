package edu.uprm.cse.datastructures.cardealer;
import java.util.*;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarTable;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;
import edu.uprm.cse.datastructures.cardealer.util.HashTableOA;

@Path("/cars")
public class CarManager {
	private final HashTableOA<Long,Car> carTable = CarTable.getInstance();


	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] getAllCars() {

		//		Car[] result = new Car[carList.size()];
		//		for (int i = 0; i < carList.size(); i++) {
		//			result[i] = carList.get(i);
		//		}
		Car[] result = new Car[carTable.getValues().size()];

		//		Iterator<Car> it = carList.getValues().iterator();
		//		int i = 0;
		//		while(it.hasNext()) {
		//			result[i] = it.next();
		//			i++;
		//		}

		for(int i=0;i<result.length;i++) {

			result[i]=carTable.getValues().get(i);
		}		
		return result;

	}


	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Car getCar(@PathParam("id") long id){
		if(carTable.contains(id)) {
			//			for (int i = 0; i < carList.size(); i++) {
			//			if (carList.get(i).getCarId() == id) {
			//				return carList.get(i);
			//				}}

			return carTable.get(id);
		}
		throw new WebApplicationException(404);
	}



	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addCar(Car car){
		if(car==null) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}

		//		carList.add(car);
		carTable.put(car.getCarId(), car);

		return Response.status(Response.Status.CREATED).build();
	}


	@PUT
	@Path("/{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateCar(Car car){
		if(car==null) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}else if(carTable.contains(car.getCarId())){
			//		for (int i = 0; i < carList.size(); i++) {
			//			if (carList.get(i).getCarId() == car.getCarId()) {
			//				carList.remove(i);
			//				carList.add(car);
			//				return Response.status(200).build();
			//			}
			//		}

			carTable.remove(car.getCarId());
			carTable.put(car.getCarId(),car);
			return Response.status(200).build();
		}
		return Response.status(Response.Status.NOT_FOUND).build();

	}


	@DELETE
	@Path("/{id}/delete")
	public Response deleteCar(@PathParam("id") long id){
		if(id>carTable.size()) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		else if(id<0) {
			return Response.status(Response.Status.BAD_REQUEST).build();

		}

		//		else {	for (int i = 0; i < carList.size(); i++) {
		//			if (carList.get(i).getCarId() == id) {
		//				carList.remove(i);
		//				return Response.status(200).build();
		//			}
		//		}

		else {
			if(carTable.contains(id)) {
				carTable.remove(id);
				return Response.status(200).build();
			}
		}
		return Response.status(Response.Status.NOT_FOUND).build();
	}
}


