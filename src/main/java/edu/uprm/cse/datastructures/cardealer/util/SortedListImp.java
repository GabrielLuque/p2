package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList.Node;


public class SortedListImp<E> implements SortedList<E> {
	
	
	private class SortedListIter<E> implements Iterator<E>{
		int next;
		public SortedListIter() {
			this.next = 0;
			
			
			
		}
		@Override
		public boolean hasNext() {
		return this.next < size();
		}

		@Override
		public E next() {
			if(this.hasNext()) {
				E result     = (E) get(this.next);
				next++;
				
				return result;
			}
			else {
				throw new NoSuchElementException();
			}
		}
		
	}
	
	
	
	
	private E[] elements;

	private static final int DEFAULT_SIZE = 10;
	public Node header;
	public Node tail;
	private int size = 0;

	private Comparator<E> comparator;
	
	public SortedListImp(Comparator E) {
		this.header = new Node<E>();
		this.tail = new Node<E>();
		this.comparator = E;
		this.size = 0;
		
		this.header.setReferenceNext(this.tail);
		this.tail.setReferencePrevious(this.header);
	}
	
	@Override
	public Iterator<E> iterator() {
		return new SortedListIter();
	}

	@Override
	public boolean add(E obj) {
		if(obj == null) {
			throw new IllegalArgumentException();
		}
		
		if(this.isEmpty()) {
			Node<E> newNode = new Node<E>(obj,this.getTail(),this.getHeader());
			
			this.getHeader().setReferenceNext(newNode);
			this.getTail().setReferencePrevious(newNode);
			
			//System.out.println("PRE-SIZE " + this.size());
			this.size++;
			//System.out.println("POST-SIZE "+this.size());
			return true;
		}
		else {
			Node<E> newNode = new Node<E>(obj,this.getTail(),this.getTail().getReferencePrevious());
			
			for(Node<E> tempNode = this.getHeader();tempNode!= this.getTail();tempNode =tempNode.getReferenceNext()) {
			if(tempNode.getData()!=null) {
			if(this.comparator.compare(obj, tempNode.getData()) <= 0){
				
				newNode.setReferenceNext(tempNode);
				newNode.setReferencePrevious(tempNode.getReferencePrevious());
				tempNode.getReferencePrevious().setReferenceNext(newNode);

				
				tempNode.setReferencePrevious(newNode);
				
				this.size++;
				return true;
			
			}}
			}	
			// System.out.println("=*=*=HEADER REFERENCE PREVIOUS: "+ this.getHeader().getReferencePrevious().getData() +" *=*=*=");
		this.size++;
			
		newNode.setReferenceNext(this.getTail());
		newNode.setReferencePrevious(this.getTail().getReferencePrevious());
		
		this.getTail().getReferencePrevious().setReferenceNext(newNode);
		
		
		this.getTail().setReferencePrevious(newNode);
		
		
		
		}
//		System.out.println("=====MEME AREA REMEMBER=======");
//		for(int i=0;i<this.elements.length;i++) {
//			
//			System.out.println(this.elements[i]);
//			
//		}
//		System.out.println("=====MEME AREA REMEMBER=======");
		
		
		return true;
	}

	@Override
	public int size() {
		
		return size;
	}

	@Override
	public boolean remove(E obj) {
		if(obj == null) {
			throw new IllegalArgumentException();
		}
		
		if(this.isEmpty()) {
			return false;
		}
		else {
			Node<E> currentNode = this.header.getReferenceNext();
		    for(int i=0;i<this.size();i++) {
		    	 if(currentNode.getData()==obj) {
		 		    currentNode.getReferencePrevious().setReferenceNext(currentNode.getReferenceNext());
		 		    currentNode.getReferenceNext().setReferencePrevious(currentNode.getReferencePrevious());
		 		    
		 		    currentNode.setReferenceNext(null);
		 		    currentNode.setReferencePrevious(null);
		 		    currentNode.setData(null);
		 		    
		 		    currentNode = null;
		 		    this.size--;
		 		    return true;
		 		   }
		    	
		    	
		    	
		    	currentNode = currentNode.getReferenceNext();
		    	
		    }
		    return false;
		  
		}
	}

	@Override
	public boolean remove(int index) {
		if(this.isEmpty()) {
			return false;
		}
		
		if(index > this.size() -1) {
			throw new IndexOutOfBoundsException("Bigger than the Linkedlist size."); 
		}
		else if(index < 0) {
			throw new IndexOutOfBoundsException("Index is less than zero. You wanna go reverse in the LinkedList? This ain't Python."); 
		}
	
		else {
			Node<E> currentNode = this.header.getReferenceNext();
		    int i=0;
			while(i!=index) {
				currentNode = currentNode.getReferenceNext();
				i++;
			}
			
			
			
		    currentNode.getReferencePrevious().setReferenceNext(currentNode.getReferenceNext());
		    currentNode.getReferenceNext().setReferencePrevious(currentNode.getReferencePrevious());
		    
		    currentNode.setReferenceNext(null);
 		    currentNode.setReferencePrevious(null);
 		    currentNode.setData(null);
		    
		    currentNode = null;
		    this.size--;
		    return true;
		}
	}

	@Override
	public int removeAll(E obj) {
		if(obj == null) {
			throw new IllegalArgumentException();
		}
		
		int count = 0;
		while(this.remove(obj)) {count++;}
		
		return count;
	}

	@Override
	public E first() {
		return (E) this.getHeader().getReferenceNext().getData();
	}
	
	@Override
	public E last() {
		return (E) this.getHeader().getReferencePrevious().getData();
	}


	@Override
	public E get(int index) {
		if(index >this.size()-1) {
			throw new IndexOutOfBoundsException("Bigger than the Linkedlist size."); 
		}
		else if(index < 0) {
			throw new IndexOutOfBoundsException("Index is less than zero. You wanna go reverse in the LinkedList? This ain't Python."); 
		}
		else {
			Node<E> result = this.getHeader().getReferenceNext();
			
			for(int i=0;i<index;i++) {
			result = result.getReferenceNext();
				
			}	
			
			return result.getData();
		}	
	}

	@Override
	public void clear() {
		while(this.remove(0)) {}
		
	}

	@Override
	public boolean contains(E e) {
		if(e == null) {
			throw new IllegalArgumentException();
		}
		
		return this.firstIndex(e) > 0;
	}

	@Override
	public boolean isEmpty() {
		return (this.size() == 0);
	}

	@Override
	public int firstIndex(E e) {
		if(e == null) {
			throw new IllegalArgumentException();
		}
		
		if(this.isEmpty()) {
			return -1;
		}
		else {
			Node<E> currentNode = this.header.getReferenceNext();
			
			for(int i = 0; i<this.size();i++) {
				if(currentNode.getData().equals(e)) {
					return i;
				}
				else {
					currentNode = currentNode.getReferenceNext();
				}
			}
		}
		return -1;
	}

	@Override
	public int lastIndex(E e) {
		if(e == null) {
			throw new IllegalArgumentException();
		}
		
//		int indext = 0;
//		Node<E> currentNodeView = this.getHeader().getReferenceNext();
//		System.out.println("======LIST======");
//		while(indext<this.size()) {
//			
//			System.out.println(currentNodeView.getData());
//			currentNodeView = currentNodeView.getReferenceNext();
//			indext++;
//			
//		}
//		System.out.println("======LIST======");
		
		if(this.isEmpty()) {
			return -1;
		}
		else {
			Node<E> currentNode = this.header.getReferencePrevious();
			int index = this.size()-1;
			while(index>0) {
				if(currentNode.getData().equals(e)) {
					return index;
				}
				index--;
				currentNode = currentNode.getReferencePrevious();
			}
		}
		return -1;
	}

	public E[] getElements() {
		return elements;
	}

	public void setElements(E[] elements) {
		this.elements = elements;
	}

	public Node getHeader() {
		return header;
	}

	public void setHeader(Node header) {
		this.header = header;
	}

	public Node getTail() {
		return tail;
	}

	public void setTail(Node tail) {
		this.tail = tail;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public Comparator getComparator() {
		return comparator;
	}

	public void setComparator(Comparator comparator) {
		this.comparator = comparator;
	}
	
	
	
	
}
