package edu.uprm.cse.datastructures.cardealer.model;

import edu.uprm.cse.datastructures.cardealer.model.*;
import edu.uprm.cse.datastructures.cardealer.util.HashTableOA;

public class CarTable {

	private static HashTableOA<Long,Car> carTable = new HashTableOA<Long,Car>(new LongComparator(),new CarComparator());


	private CarTable() {
		
		CarTable.carTable = new HashTableOA<Long,Car>(new LongComparator(),new CarComparator());
	}

	public static HashTableOA<Long,Car> getInstance(){
		return carTable;
	}


	public static void resetCars() {
		carTable = new HashTableOA<Long,Car>(new LongComparator(),new CarComparator());
	}


}
